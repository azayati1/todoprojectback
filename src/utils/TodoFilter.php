<?php

namespace App\utils;

class TodoFilter
{
    private string $state;
    private string $name;

    /**
     * TodoFilter constructor.
     */
    public function __construct()
    {
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): void
    {
        $this->state = $state;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
