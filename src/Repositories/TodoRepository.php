<?php

namespace App\Repositories;

use App\Document\ToDo;
use App\utils\TodoFilter;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Bundle\MongoDBBundle\Repository\ServiceDocumentRepository;
use Doctrine\ODM\MongoDB\MongoDBException;
use Exception;

class TodoRepository extends ServiceDocumentRepository
{
    /**
     * TodoRepository constructor.
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ToDo::class);
    }

    /**
     * @return array<ToDo>
     *
     * @throws Exception
     */
    public function findByStateAndName(TodoFilter $data): array
    {
        try {
            $filter = [];
            //    $qb = $this->dm->createQueryBuilder(ToDo::class);
            if ('x' != $data->getState()) {
                $filter['state'] = $data->getState();
                //        $qb->addAnd($qb->expr()->field('state')->equals($data->getState()));
            }
            if ('' != $data->getName()) {
                $filter['name'] = $data->$data->getName();
                //       $qb->addAnd($qb->expr()->field('name')->equals($data->getName()));
            }

            // return $qb->getQuery()->execute()->toArray();
            return $this->findBy($filter);
        } catch (MongoDBException | Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function addTodo(ToDo $todo): ToDo
    {
        try {
            $this->dm->persist($todo);
            $this->dm->flush();

            return $todo;
        } catch (MongoDBException | Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function updateTodo(Todo $todo): ToDo
    {
        try {
            $this->dm->persist($todo);
            $this->dm->flush();

            return $todo;
        } catch (MongoDBException | Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function removeTodo(string $id): void
    {
        try {
            $todo = $this->dm->getRepository(ToDo::class)->find($id);
            $this->dm->remove($todo);
            $this->dm->flush();
        } catch (MongoDBException | Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
