<?php

namespace App\Controller;

use App\Document\ToDo;
use App\Repositories\TodoRepository;
use App\utils\TodoFilter;
use Doctrine\ODM\MongoDB\DocumentManager;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ToDoController extends AbstractController
{
    private DocumentManager $dm;
    private SerializerInterface $serializer;
    private TodoRepository $todoRepository;

    public function __construct(
        DocumentManager $dm,
        SerializerInterface $serializer,
        TodoRepository $todoRepository)
    {
        $this->dm = $dm;
        $this->serializer = $serializer;
        $this->todoRepository = $todoRepository;
    }

    /**
     * @Route("/todo", name="app_article_sw",methods={"POST"})
     */
    public function addTodo(Request $request): Response
    {
        try {
            $data = $this->serializer->deserialize(strval($request->getContent()), ToDo::class, 'json');
            $todo = $this->todoRepository->addTodo($data);

            return new Response($this->serializer->serialize($todo, 'json'));
        } catch (Exception $e) {
            return new Response('Error while adding Todo '.'message:'.$e->getMessage(),400);
        }
    }

    /**
     * @Route("/todo", name="ap_artie_show",methods={"PUT"})
     */
    public function updateTodo(Request $request): Response
    {
        try {
            $oldTodo = $this->dm->getRepository(ToDo::class)->find($request->get('id'));
            $data = $this->serializer->deserialize(strval($request->getContent()), ToDo::class, 'json', [AbstractNormalizer::OBJECT_TO_POPULATE => $oldTodo]);
            $todo = $this->todoRepository->updateTodo($data);

            return new Response($this->serializer->serialize($todo, 'json'));
        } catch (Exception $e) {
            return new Response('Error while updating Todo '.'message:'.$e->getMessage());
        }
    }

    /**
     * @Route("/todo/{id}", name="app_articlshow",methods={"GET"})
     */
    public function getTodo(string $id): Response
    {
        $todo = $this->dm->getRepository(ToDo::class)->find($id);
        $data = $this->serializer->serialize($todo, 'json');

        return new Response($data);
    }

    /**
     * @Route("/todos", name="todos_get",methods={"POST"})
     */
    public function getTodos(Request $request): Response
    {
        try {
            $filter = $this->serializer->deserialize(strval($request->getContent()), TodoFilter::class, 'json');
            $todo = $this->todoRepository->findByStateAndName($filter);

            return new Response($this->serializer->serialize($todo, 'json'));
        } catch (Exception $e) {
            return new Response('Error while loading Todos . message:'.$e->getMessage());
        }
    }

    /**
     * @Route("/todo/{id}", name="app_articlsw",methods={"DELETE"})
     */
    public function deleteTodo(string $id): Response
    {
        try {
            $this->todoRepository->removeTodo($id);

            return new Response($id);
        } catch (Exception $e) {
            return new Response('Error while deleting Todo with id: '.$id.'message:'.$e->getMessage());
        }
    }

    /**
     * @Route("/todo/{id}", name="ap_artiehdcccow",methods={"PATCH"})
     */
    public function updateTodoState(Request $request, string $id): Response
    {
        try {
            $data = json_decode(strval($request->getContent()), true);
            $todo = $this->dm->getRepository(ToDo::class)->find($id);
            $todo->setState($data['state']);
            $this->dm->persist($todo);
            $this->dm->flush();

            return new Response($this->serializer->serialize($todo, 'json'));
        } catch (Exception $e) {
            return new Response('Error while updating Todo '.'message:'.$e->getMessage());
        }
    }
}
