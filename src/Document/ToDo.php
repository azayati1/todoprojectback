<?php


namespace App\Document;
use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(db="todo", collection="Todo",repositoryClass="App\Repositories\TodoRepository")
 */
class ToDo
{
    /**
     * @MongoDB\Id
     */
    protected ?string $id=null;

    /**
     * @MongoDB\Field(type="string")
     */
    protected ?string $name=null;


    /**
     * @MongoDB\Field(type="string")
     */
    protected string $description;

    /**
     * @MongoDB\Field(type="date")
     */
    protected ?DateTime $creationDate=null;

    /**
     * @MongoDB\Field(type="int")
     */
    protected ?int $duration=null;

    /**
     * @MongoDB\Field(type="string")
     */
    protected string $state;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     */
    public function setCreationDate($creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    public function __toString() { return 'I CAUSE THIS ISSUE';}


}